//
//  CharacterTableViewCell.swift
//  Marvel
//
//  Created by Willian Antunes on 09/09/18.
//  Copyright © 2018 Willian Antunes. All rights reserved.
//

import UIKit

class CharacterTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!

}
