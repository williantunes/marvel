//
//  ComicTableViewCell.swift
//  Marvel
//
//  Created by Willian Antunes on 10/09/18.
//  Copyright © 2018 Willian Antunes. All rights reserved.
//

import UIKit

class ComicTableViewCell: UITableViewCell {

    @IBOutlet weak var comicName: UILabel!

}
