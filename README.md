# MARVEL Characters Catalog

This app shows a list of Marvel characters with a details screen detailing the comics in which the characters appears. The characters data comes from Marvel's public  API. In order to access the service, a private key is required, anyone can get a key at developer.marvel.com, since Marvel recommends not to display the private key in public repositories, you add your own key no the code in the files: ViewController.swift, CharacterDetailViewController.swift and MarvelTests.swift.

I did unit tests for the marvel service as well as the md5 digest function which is required to create the url to access the service. The code coverage is 56% according to Xcode.
