//
//  ComicsResponse.swift
//  Marvel
//
//  Created by Willian Antunes on 10/09/18.
//  Copyright © 2018 Willian Antunes. All rights reserved.
//

import Foundation

struct ComicsResponse: Codable {
    
    var code: Int?
    var status: String?
    var data: ComicDataContainer?
    
}

struct ComicDataContainer: Codable {
    
    var offset: Int?
    var limit: Int?
    var total: Int?
    var count: Int?
    var results: [Comic]?
    
}

struct Comic: Codable {
    
    var id: Int?
    var title: String?
    var issueNumber: Double?
    
}
