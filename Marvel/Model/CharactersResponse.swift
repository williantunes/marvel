//
//  CharactersResponse.swift
//  Marvel
//
//  Created by Willian Antunes on 09/09/18.
//  Copyright © 2018 Willian Antunes. All rights reserved.
//

import Foundation

struct CharactersResponse: Codable {
    
    var code: Int?
    var status: String?
    var data: CharacterDataContainer?
    
}

struct CharacterDataContainer: Codable {
    
    var offset: Int?
    var limit: Int?
    var total: Int?
    var count: Int?
    var results: [Character]?
    
}

struct Character: Codable {
    
    var id: Int?
    var name: String?
    var description: String?
    var thumbnail: Thumbnail?
    var image: Data?
    
}

struct Thumbnail: Codable {
    
    let path: String?
    let ext: String?
    
    enum CodingKeys: String, CodingKey {
        case path
        case ext = "extension"
    }
    
}
