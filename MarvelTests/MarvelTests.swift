//
//  MarvelTests.swift
//  MarvelTests
//
//  Created by Willian Antunes on 09/09/18.
//  Copyright © 2018 Willian Antunes. All rights reserved.
//

import XCTest
@testable import Marvel

class MarvelTests: XCTestCase {
    
    var sessionUnderTest: URLSession!
    
    override func setUp() {
        super.setUp()
        sessionUnderTest = URLSession(configuration: URLSessionConfiguration.default)
    }

    override func tearDown() {
        sessionUnderTest = nil
        super.tearDown()
    }
    
    func testMd5Digest() {
        let str = "abcd"
        let expectedMd5 = "e2fc714c4727ee9395f324cd2e7f331f"
        XCTAssert(str.md5 == expectedMd5)
    }
    
    func testValidateCallToMarvelService() {
        let apiKey = ""
        let privateKey = ""
        let timeStamp = String(Date().timeIntervalSince1970)
        let hash = (timeStamp + privateKey + apiKey).md5
        
        let promise = expectation(description: "Status code: 200")

        guard let url = URL(string: "https://gateway.marvel.com/v1/public/characters?ts=" + timeStamp + "&apikey=" + apiKey + "&hash=" + hash) else { return }

        let dataTask = sessionUnderTest.dataTask(with: url) { data, response, error in

            if let error = error {
                XCTFail("Error: \(error.localizedDescription)")
                return
            } else if let statusCode = (response as? HTTPURLResponse)?.statusCode {
                if statusCode == 200 {
                    promise.fulfill()
                } else {
                    XCTFail("Status code: \(statusCode)")
                }
            }
        }
        dataTask.resume()

        waitForExpectations(timeout: 5, handler: nil)
        
    }

}
