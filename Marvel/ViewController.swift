//
//  ViewController.swift
//  Marvel
//
//  Created by Willian Antunes on 09/09/18.
//  Copyright © 2018 Willian Antunes. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var charactersTableView: UITableView!
    
    let apiKey = "d5f5160f85ba698ea759d840649ee0aa"
    let privateKey = "561f21602bc105cc5fa6ea2faf3a398276722c76"
    
    let baseUrlString = "https://gateway.marvel.com/v1/public/characters?limit=100&ts="
    
    var characters: [Character] = []
    var total: Int?
    
    var loadingCharacters = false
    
    let spinner = UIActivityIndicatorView(style: .gray)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        charactersTableView.delegate = self
        charactersTableView.dataSource = self
        
        spinner.startAnimating()
        spinner.frame = CGRect(x: 0, y: 0, width: charactersTableView.frame.width, height: 44)
        charactersTableView.tableFooterView = spinner;

        loadData(index: 0)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.prefersLargeTitles = true
    }
    
    func updateData() {
        DispatchQueue.main.async {
            self.charactersTableView.reloadData()
            self.charactersTableView.tableFooterView = nil
        }
    }
    
    func loadData(index: Int) {
        
        let offset = String(index)
        let timeStamp = String(Date().timeIntervalSince1970)
        let hash = (timeStamp + privateKey + apiKey).md5
        
        guard let url = URL(string: baseUrlString + timeStamp + "&apikey=" + apiKey + "&hash=" + hash + "&offset=" + offset) else { return }
        
        let session = URLSession.shared

        let task = session.dataTask(with: url) { (data, response, error) in

            if let _ = error {
                return
            } else {
                if let data = data {

                    let decoder = JSONDecoder()

                    guard let charactersResponse = try? decoder.decode(CharactersResponse.self, from: data) else { return }

                    self.total = charactersResponse.data!.total
                    self.characters.append(contentsOf: charactersResponse.data!.results!)
                    self.loadingCharacters = false
                    self.updateData()
                }
            }
        }
        task.resume()
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? CharacterDetailViewController, segue.identifier == "characterDetails" {
            guard let character = sender as? Character else { return }
            vc.character = character
        }
    }


}

// MARK: - TableView Methods

extension ViewController {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return characters.count
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if (indexPath.row == self.characters.count - 1) {
            if characters.count < self.total! && loadingCharacters == false {
                loadingCharacters = true
                charactersTableView.tableFooterView = spinner
                loadData(index: characters.count)
            } else if characters.count == self.total! {
                charactersTableView.tableFooterView = nil
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "characterCell", for: indexPath) as! CharacterTableViewCell
        
        cell.nameLabel.text = characters[indexPath.row].name
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let character = characters[indexPath.row]
        performSegue(withIdentifier: "characterDetails", sender: character)
    }
    
}

