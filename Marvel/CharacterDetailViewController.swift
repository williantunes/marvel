//
//  CharacterDetailViewController.swift
//  Marvel
//
//  Created by Willian Antunes on 09/09/18.
//  Copyright © 2018 Willian Antunes. All rights reserved.
//

import UIKit

class CharacterDetailViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var character: Character!
    
    @IBOutlet weak var characterImage: UIImageView!
    @IBOutlet weak var characterDescription: UILabel!
    @IBOutlet weak var comicsTableView: UITableView!
    
    let apiKey = "d5f5160f85ba698ea759d840649ee0aa"
    let privateKey = "561f21602bc105cc5fa6ea2faf3a398276722c76"
    
    let baseUrlString = "https://gateway.marvel.com/v1/public/characters/"
    
    var comics: [Comic] = []
    var total: Int?
    
    var loadingComics = false
    var pendingUpdate = false
    
    let spinner = UIActivityIndicatorView(style: .gray)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        comicsTableView.dataSource = self
        comicsTableView.delegate = self
        self.title = character.name
        self.navigationController?.navigationBar.prefersLargeTitles = false
        if character.description == "" {
            characterDescription.text = "No description available."
        } else {
            characterDescription.text = character.description
        }
        
        spinner.startAnimating()
        spinner.frame = CGRect(x: 0, y: 0, width: comicsTableView.frame.width, height: 44)
        comicsTableView.tableFooterView = spinner
        
        loadImage(imageUrl: character.thumbnail!.path! + "." + character.thumbnail!.ext!)
        loadData(index: 0)
    }
    
    func loadImage(imageUrl: String) {
        
        let session = URLSession.shared
        
        guard let url = URL(string: imageUrl) else { return }
        
        let task = session.dataTask(with: url) { (data, response, error) in
            
            if let _ = error {
                return
            } else {
                if let data = data {
                    self.character.image = data
                    self.updateImage()
                }
            }
        }
        task.resume()
        
    }
    
    func updateImage() {
        DispatchQueue.main.async {
            self.characterImage.image = UIImage(data: self.character.image!)
            self.characterImage.layer.cornerRadius = self.characterImage.frame.size.height / 2
            self.characterImage.clipsToBounds = true
        }
    }
    
    func updateData() {
        DispatchQueue.main.async {
            self.comicsTableView.tableFooterView = nil
            self.comicsTableView.reloadData()
        }
    }
    
    func loadData(index: Int) {
        
        let offset = String(index)
        let timeStamp = String(Date().timeIntervalSince1970)
        let hash = (timeStamp + privateKey + apiKey).md5
        let urlString = baseUrlString + String(self.character.id!) + "/comics?limit=100&ts=" + timeStamp + "&apikey=" + apiKey + "&hash=" + hash + "&offset=" + offset
        
        guard let url = URL(string: urlString) else { return }
        
        let session = URLSession.shared
        
        let task = session.dataTask(with: url) { (data, response, error) in
            
            if let _ = error {
                return
            } else {
                if let data = data {
                    
                    let decoder = JSONDecoder()
                    
                    guard let comicsResponse = try? decoder.decode(ComicsResponse.self, from: data) else { return }
                    
                    self.total = comicsResponse.data!.total
                    self.comics.append(contentsOf: comicsResponse.data!.results!)
                    self.loadingComics = false
                    self.updateData()
                }
            }
        }
        task.resume()
        
    }

}

// MARK: - TableView Methods

extension CharacterDetailViewController {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "comicCell") as! ComicTableViewCell
        
        cell.comicName.text = comics[indexPath.row].title!
        
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return comics.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Comics"
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if (indexPath.row == self.comics.count - 1) {
            if comics.count < self.total! && loadingComics == false {
                loadingComics = true
                comicsTableView.tableFooterView = spinner
                loadData(index: comics.count)
            } else if comics.count == self.total! {
                comicsTableView.tableFooterView = nil
            }
        }
    }
    
    
    
}
